import api from './Api';
import Cookie from 'js-cookie';

export default {
    getCookie () {
        let token = Cookie.get('XSRF-TOKEN');
        if (token) {
            return new Promise(resolve => {
                resolve(token);
            });
        }
        console.log(api.get('/sanctum/csrf-cookie'))

        return api.get('/sanctum/csrf-cookie')
    }
}