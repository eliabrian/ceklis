import axios from 'axios';

let api = axios.create({
    baseURL: 'https://api.ceklis.xyz/',
})

api.defaults.withCredentials = true;
api.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

export default api;