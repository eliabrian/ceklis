import api from './Api';
import csrf from './Csrf';

export default {
    async register (form) 
    {
        await csrf.getCookie();

        return api.post('/api/register', form)
    },

    async login (form) 
    {
        await csrf.getCookie();
    
        return api.post("/api/login", form);
    },

    async logout (id) {
        await csrf.getCookie();
        
        let config = {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
            }
        }

        return api.post(`/api/${id}/logout`, config);
    },

    auth (id) {
        let config = {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
            }
        }
        return api.get(`/api/users/${id}`, config);
    },

    edit (id, form) {
        let config = {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
            }
        }

        return api.post(`/api/users/${id}?_method=PATCH`, {name: form.name, email: form.email}, config)
    },

    delete (id) {

        let config = {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
            }
        }

        return api.delete(`/api/users/${id}`, config);
    }
}
